<html>
<head>
<meta name="description" content="Ofrecemos un gran numero de servicios para apoyarte en todos tus proyectos dentro de todos los niveles educativos, tambien contamos con tutorias de refuerzo para que comprendas todas las tematicas de tus clases y tengas las mejores calificaciones." />

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<meta name="keywords" content="Clases de programacion,Programacion,Clases de matematicas,WildKath">
<meta name="author" content="Katherine Beltran,Wilder Herrera">
<link rel="icon" href="../images/pencilicon.png" type="image/x-icon"/>
<meta name="theme-color" content="#081926" />
	<title>WildKath Tareas</title>

	<!--meta tags -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="Versed Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); }
	</script>
	<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
	<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
	<!--//meta tags ends here-->
	<!--booststrap-->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
   <link rel="stylesheet" href="css/animations.css">
	<!--//booststrap end-->

	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //font-awesome icons -->
	<!--stylesheets-->
	<link href="css/style.css" rel='stylesheet' type='text/css' media="all">
	<link rel="stylesheet" href="css/lightbox.css">

	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
	<!-- banner text slider-->

	<link href="//fonts.googleapis.com/css?family=Dosis:300,400,500" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<!--//style sheet end here-->
</head>
<body>





<div class="row">
<div class="col-md-14">
	<div class="banner-w31">
  	<div class="w3-agile-logo">
			<div class=" head-wl">
				
				   <div class="row" >

						<div class="col-md-5 col-xs-8 text-left" >
							 <div class="headder-w3">
									<h1><a href="index.php"><img src="images/pencilicon.png" style="width: 8%;height: 8%"><span id="titulo"> WILDKATH Tareas</span></a>
									</h1>	
									<p id="ubi" style="color:white;margin-left:15%;"><span class="fa fa-map-marker cap"></span>   Villavicencio-Meta</p>
							</div>
							</div>

							<div class="col-md-5" id="telefono">
								<table class="table  text-center" style="margin-left: 30%;margin-bottom: 0%;"> 
								<tr>
								<td>
								<h1 style="text-align:left;"><p style="font-size:40%;color: white"><span  class="fa fa-whatsapp icons-left" aria-hidden="true"></span>Llámanos:<span class="number">3209720220</span></p></h1>
								</td>
								</tr>
								<tr>
								<td style="margin-bottom: 0%;"><h1 style="text-align:left;"><p style="font-size:40%;color:white"><span class="fa fa-envelope icons-left" aria-hidden="true"></span>Escribenos:  <span class="number">    wildkath.ingenieria@gmail.com
								</td>
								</tr>

								
								
								</table>		
							</div>
	

					</div>
				   

				

				

				<div class="clearfix"> </div>
			</div>
			<div class="agileits-banner-grids text-center" >

				<div class="banner-bottom-girds text-center">
				

					<div class="col-md-4 col-sm-4 clr13 agileits-banner-grid text-center col-md-offset-4" style="background-color: rgba(173, 173, 173, 0.8);;border-radius: 5%;">
						<span style="color: black;font-size: 110%">Correo enviado</span>
						<a href="index.php"><p><span class="fa fa-home banner-icon" aria-hidden="true"></span></p></a>
						<h4 style="color:black;"> Inicio</h4>
						
						
					</div>




				
			</div>
		</div>
</div>

<?php
     /*if(isset($_FILES['image'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_size = $_FILES['image']['size'];
      $file_tmp = $_FILES['image']['tmp_name'];
      $file_type = $_FILES['image']['type'];
      $a=explode('.',$_FILES['image']['name']);
      $file_ext=strtolower(array_pop($a));
      $expensions= array("jpeg","jpg","png","pdf");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a PDF, JPEG or PNG file.";
      }
      
      if($file_size > 2097152) {
         $errors[]='File size must be excately 2 MB';
      }
      
       move_uploaded_file($file_tmp,"subidas/".$file_name); //The folder where you would like your file to be saved
     }*/



            

			$nombre=$_POST["nombre"];
			$apellido=$_POST["apellido"];
			$correo=$_POST["correo"];
			$telefono=$_POST["telefono"];
			
			$descripcion=$_POST["descripcion"];
			
			require ('vendor/phpmailer/phpmailer/PHPMailerAutoload.php');
			$mail = new PHPMailer();
		 	$mail2= new PHPMailer();
		 	$mail2->debug=2;
			$mail2->IsSMTP();                      	                // Set mailer to use SMTP
			$mail2->Host = 'smtp.gmail.com';                 // Specify main and backup server
			$mail2->Port = 587;                                    // Set the SMTP port
			$mail2->SMTPAuth = true;                               // Enable SMTP authentication
			$mail2->Username = 'wildkath.ingenieria@gmail.com';                // SMTP username
			$mail2->Password = '20532050';                  // SMTP password
			$mail2->SMTPSecure = 'tls';     


			                           // Enable encryption, 'ssl' also accepted
			$mail2->From ='wildkath.ingenieria@gmail.com';
			$mail2->FromName = '¡Hola!'.''.$nombre.' '.$apellido;
			$mail2->AddAddress($correo);
			$mail2->IsHTML(true);                                  // Set email format to HTML
			$mail2->Subject = 'WildKath Tareas';
			$mail2->Body    = "Muchas gracias por usar nuestros servicios, Hemos recibido tu solicitud y en poco tiempo nos estaremos comunicando contigo.";
			
			if(!$mail2->Send()) {
			   echo 'No se pudo enviar el mensaje';
			   echo 'Mailer Error: ' . $mail2->ErrorInfo;
			   exit;
			}
			


			$mail->IsSMTP(); 
			$mail->debug=2;                                     // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';                 // Specify main and backup server
			$mail->Port = 587;                                    // Set the SMTP port
			$mail->SMTPAuth = true;                                // Enable SMTP authentication
			$mail->Username = 'wildkath.ingenieria@gmail.com';                // SMTP username
			$mail->Password = '20532050';                  // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted		
			                                        // Enable encryption, 'ssl' also accepted
			$mail->From ='wildkath.ingenieria@gmail.com';
			$mail->FromName = $nombre.' '.$apellido;

			if(isset($_FILES) && (bool) $_FILES) {
  
				$allowedExtensions = array("pdf","doc","docx","gif","jpeg","jpg","png","rtf","txt");
				
				$files = array();
				foreach($_FILES["image"]["error"]  as $key=>$error) {
					$name=$_FILES["image"]["name"][$key];
					$tmp=$_FILES["image"]["tmp_name"][$key];					
					move_uploaded_file($tmp,"subidas/".$name);
					$mail->AddAttachment("subidas/".$name);
				}
			}
			


			$mail->AddAddress('wildkath.ingenieria@gmail.com');
			$mail->IsHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'WildKath Tareas';
			$mail->Body    = " ".$descripcion." ".$telefono;
			
			if(!$mail->Send()) {
			   echo 'Message could not be sent.';
			   echo 'Mailer Error: ' . $mail->ErrorInfo;
			   exit;
			}
			echo 'Enviado...';
?>
</body>
	<script type='text/javascript' src='js/jquery-2.2.3.min.js'></script>
	<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<!-- //js  working-->
	<!--FlexSlider-->

	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">

		$(window).load(function() {
			  $('.flexslider').flexslider({
			    animation:  Modernizr.touch ? "slide" : "fade",
			    smoothHeight:true

			  });
			  $(".loader1").fadeOut("slow");
			
						    $('.flexslider1').flexslider({
				    animation: "slide",
				    controlNav: "thumbnails"
				  });
			})



	</script>
	<!--End-slider-script-->
	<!--gallery script-->
	<script src="js/lightbox-plus-jquery.min.js"></script>
	<!--//gallery script-->


	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({ scrollTop: $(this.hash).offset().top }, 900);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		
		function hola(){
			document.getElementById("temasf").value = "";
		}


$(window).scroll(function() {

		$('#como').each(function(){
			var imagePos = $(this).offset().top;
			
			var topOfWindow = $(window).scrollTop();
				if (imagePos < topOfWindow+400) {
					$(this).addClass("slideExpandUp");
				}
			});	

$('#como3').each(function(){
			var imagePos = $(this).offset().top;
			
			var topOfWindow = $(window).scrollTop();
				if (imagePos < topOfWindow+400) {
					$(this).addClass("slideExpandUp");
				}
			});	

$('#como1').each(function(){
		var imagePos = $(this).offset().top;
			
			var topOfWindow = $(window).scrollTop();
				if (imagePos < topOfWindow+400) {
					$(this).addClass("slideUp");
				}
			});



});	
	


	


	</script>

</html>
﻿<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="es">

<head>
<meta name="description" content="Ofrecemos un gran numero de servicios para apoyarte en todos tus proyectos dentro de todos los niveles educativos, tambien contamos con tutorias de refuerzo para que comprendas todas las tematicas de tus clases y tengas las mejores calificaciones." />

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<meta name="keywords" content="Clases de programacion,Programacion,Clases de matematicas,WildKath">
<meta name="author" content="Katherine Beltran,Wilder Herrera">
<link rel="icon" href="../images/pencilicon.png" type="image/x-icon"/>
<meta name="theme-color" content="#081926" />
	<title>WildKath Tareas</title>

	<!--meta tags -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="Versed Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); }
	</script>
	<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
	<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
	<!--//meta tags ends here-->
	<!--booststrap-->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
   <link rel="stylesheet" href="css/animations.css">
	<!--//booststrap end-->

	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //font-awesome icons -->
	<!--stylesheets-->
	<link href="font/style.css" rel='stylesheet' type='text/css' media="all">
	<link href="css/style.css" rel='stylesheet' type='text/css' media="all">
	<link rel="stylesheet" href="css/lightbox.css">

	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
	<!-- banner text slider-->

	<link href="//fonts.googleapis.com/css?family=Dosis:300,400,500" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<!--//style sheet end here-->
</head>

<body>

<script>
 $('#animatedElement').click(function() {
 $(this).addClass("slideUp");
 });
</script>

<div class="container">
<div class="loader text-center" id="loader">
	<h1>Cargando...</h1>

</div>
</div>



<div class="row">
<div class="col-md-14">
	<div class="banner-w3">
  	<div class="w3-agile-logo">
			<div class=" head-wl">
				
				   <div class="row" >

						<div class="col-md-5 col-xs-8 text-left" >
							 <div class="headder-w3">
									<h1><a href="index.html"><img src="images/pencilicon.png" style="width: 8%;height: 8%"></span><span id="titulo"> WILDKATH Tareas</span></a>
									</h1>	
									<p id="ubi" style="color:white;margin-left:15%;"><span class="fa fa-map-marker cap"></span>   Villavicencio-Meta</p>
							</div>
							</div>

						<div class="col-md-2 col-xs-2 col-md-offset-2 col-xs-offset-2">

								<div class="navbar-header">
									   			 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
									                <span class="sr-only">Toggle navigation</span>
									                <span class="icon-bar"></span>
									                <span class="icon-bar"></span>
									                <span class="icon-bar"></span>
								                </button>
								              </div>

						</div>		              

							<div class="col-md-5 col-xs-5" id="telefono">
								<table class="table  text-center" style="margin-left: 30%;margin-bottom: 0%;"> 
								<tr>
								<td>
								<h1 style="text-align:left;"><p style="font-size:40%;color: white"><span  class="fa fa-whatsapp icons-left" aria-hidden="true"></span>Llámanos:<span class="number">3209720220</span></p></h1>
								</td>
								</tr>
								<tr>
								<td style="margin-bottom: 0%;"><h1 style="text-align:left;"><p style="font-size:40%;color:white"><span class="fa fa-envelope icons-left" aria-hidden="true"></span>Escribenos:  <span class="number">    wildkath.ingenieria@gmail.com
								</td>
								</tr>

								
								
								</table>		
							</div>
	

					</div>
				   
				

				<div class="clearfix"> </div>
			</div>
		</div>


		<div class="top-nav" >
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
	
	<!-- //header -->

			
				<div id="navbar" class="navbar-collapse collapse" style="margin-left:6%;">
					<ul class="nav navbar-nav" > 
						<li><a href="index.html" class="scroll"><span class="fa fa-home"></span> Inicio</a></li>
						<li><a href="#about" class="scroll"><span class="fa fa-user"></span> Nosotros</a></li>
						<li><a href="#services" class="scroll"><span class="fa fa-book"></span> Servicios</a></li>
						<li><a href="#gallery" class="scroll"><span class="fa fa-image"></span> Galeria</a></li>
						<li><a href="#contact" class="scroll"><span class="fa fa-question-circle"></span> Como funciona </a></li>
					</ul>
				</div>
			
			</div>
			</nav>	
		</div>

		<div class="container">

			<header>
			
				<div class="flexslider-info">
					<section class="slider">
						<div class="flexslider">
							<ul class="slides">
								<li>
									<div class="w3l-info">
										<h4>Conocenos</h4>
										<p>En WildKath te apoyamos para que logres los mejores resultados en tu proceso academico.</p>
										<div class="w3layouts_more-buttn">
											<a href="#about" class="scroll">Quienes somos</a>
										</div>
									</div>
								</li> 
								<li>
									<div class="w3l-info">
										<h4> Te ofrecemos un gran numero de </h4>
										<h4>soluciones</h4>
										<p> Clases,Tareas,Trabajos y Proyectos.Conoce todas las maneras en las que podemos ayudarte</p>
										<div class="w3layouts_more-buttn">
											<a href="#services" class="scroll">Servicios</a>
										</div>
									</div>
								</li>
								<li>
									<div class="w3l-info">
										<h4>Contáctanos</h4>
										<p>Dejanos un mensaje para poder comunicarnos contigo y hacer la gestion de tus tareas,proyectos o refuerzos.</p>
										<div class="w3layouts_more-buttn">
											<a href="#contacto" class="scroll">Contacto</a>
										</div>
									</div>
								</li>
							</ul>
					<ul class="flex-direction-nav">
						<li><a class="flex-prev" href="#">Previous</a></li>
						<li><a class="flex-next" href="#">Next</a></li>
					</ul>
						</div>
					</section>
				</div>
			</header>


		</div>
		<div class="clearfix"> </div>
	</div>
</div>
</div>

	<!-- //header -->
	<!-- banner-text -->

	<!-- modal -->
	




	<div class="modal about-modal fade" id="myModal1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title1 text-center" id="totulo">Clases de refuerzo</h4>
				</div>	
				<div class="modal-body">
				
					<div class="out-info text-center" >
						<img src="images/clases.jpg" alt="" />
						
						<p class="text-justify" style="color:black"><span class="fa fa-long-arrow-right icons-left" aria-hidden="true"></span>Si tienes problemas para entender los temas de alguna de tus materias, nosotros te ofrecemos clases de refuerzo para que obtengas las mejores calificaciones en tus evaluaciones. </p>

							
							<a href="clases.html" style="color:black"><input id="boton" type="button" value="Solicita tu clase de refuerzo" class="text-center"></a>
					</div>	
				</div>
			</div>
		</div>
	</div>



	<div class="modal about-modal fade" id="myModal2" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title1"  id="totulo">Tareas y trabajos</h4>
				</div>	
				<div class="modal-body">
				
					<div class="out-info text-center">
						<img src="images/proyectos.jpg" alt="" />
						
						<p  class="text-justify" style="color:black"><span class="fa fa-long-arrow-right icons-left" aria-hidden="true"></span>¿En tu colegio o universidad te dejan demasiado trabajo?, Nosotros podemos hacerlo por ti. Cuéntanos esa tarea que tanto te preocupa y nuestro equipo se encargara de resolverlo en el menor tiempo posible.</p>


							
							<a href="tarea.html" style="color:black"><input id="boton" type="button" value="Nosotros nos encargamos" class="text-center"></a>
					</div>	
				</div>
			</div>
		</div>
	</div>




		<div class="modal about-modal fade" id="myModal3" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title1"  id="totulo">Proyectos</h4>
				</div>	
				<div class="modal-body">
				
					<div class="out-info text-center">
						<img src="images/proyectos1.jpg" alt="" />
						
						<p class="text-justify" style="color:black"><span class="fa fa-long-arrow-right icons-left" aria-hidden="true" ></span>¿Te dejaron un proyecto súper complicado?, ¿dependes de esa nota para pasar la materia?, podemos hacer que sea sencillo para ti. Cuéntanos todos los detalles, lo haremos rápido y con la mejor calidad. </p>

							<a href="proyectos.html" style="color:black"><input id="boton" type="button" value="Comunicate con nosotros" class="text-center"></a>
					</div>	
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->
	<!-- //banner-text end -->
	<!--about-->
	<div class="about " id="about">
		<div class="container">
			<div id="cuete1">
			
		
			<h3 class="title clr-white" > Nosotros </h3>
			<img  id="cuete3" src="../images/cuete3.gif">
				</div>
			<div class="upper-about" id="como3">
				<h2>¿Que es WildKath?</h2>
				<p style="background-color: transparent;">En WildKath podrás encontrar apoyo en la solución de tus tareas y proyectos dentro de todos los niveles educativos, nuestro equipo de trabajo cuenta con experiencia en diferentes áreas como: Programación, Electrónica, Automatización Industrial, matemáticas , Física, Algebra y muchas otras.
				</p>

				<p>
				Adicionalmente ofrecemos clases de refuerzo para que logres comprender las temáticas mas complicadas de tus materias y alcances las mejores calificaciones.
				</p>
			</div>
			<hr>
			
			<div class="col-md-10 text-center col-md-offset-1" id="fonfo">
				<span class="fa fa-graduation-cap banner-icon"></span>
				<h1> Nuestras áreas de trabajo</h1>	
				<p> </p>
				<br>
				<table class="table text-center">
						<tr>

						<td>Algebra</td>
						<td>Física</td>
						</tr>
						
						</tr>
						<tr>
						
						<td>Algebra Lineal</td>
						<td>Matemáticas para niños</td>
						</tr>

						<tr>
						<td>Automatización Industrial</td>
						<td>Matemáticas, calculo  y Trigonometría</td>	
						</tr>	

						<tr>
						<td>Cartografía</td>
						<td>Probabilidad y estadística</td>	
						</tr>		
						
						<tr>
						<td>Diseño de paginas Web</td>
						<td>Programación</td>
						
						</tr>
						
						<tr>
						<td>Ecuaciones Diferenciales</td>
						<td>Refuerzos para primaria y bachillerato</td>
						</tr>

						<tr>
						<td>MatLab</td>
						<td>LabView</td>
						</tr>


						<tr>
						<td>Matematicas Financieras</td>
						<td>Analisis de señales</td>
						</tr>


						<tr>
						<td>
						Electrónica
						</td>
						<td>
						Trigonometría
						</td>
						</tr>

						</table></div>
		<!--	<div class="agileits-banner-grids text-center">
				<div class="banner-bottom-girds text-center">
					<div class="col-md-4 col-sm-4 clr3 agileits-banner-grid text-center col-md-offset-4 col-sm-offset-4">
						<span class="fa fa-graduation-cap banner-icon" aria-hidden="true"></span>
						<h4> Areas de trabajo</h4>
						<p> Conoce todas nuestras areas de trabajo y contactanos si piensas que podemos ayudarte con tus proyectos.</p>
						<a class="w3_play_icon1" href="#" data-toggle="modal" data-target="#myModal"> Ver todas las areas</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		-->
		</div>
	</div>
	<!--//about-->
	<!--services-section-->
	<div class="services" id="services">

		<div class="container">
		<div class="row">
			<h3 class="title" style="color:white">¿ Como podemos ayudarte ?</h3>
			<p class="text-center" style="color:white;font-size: 20px"> Te ofrecemos un gran numero de soluciones para que puedas mejorar tu rendimiento academico.</p>
			<hr class="hr-danger" style="border-top: 3px double #dedede;"/>
			
			<div class="advantages-grids" id="como1">
				<!--
				<div class="col-md-12 upper-about">
				
					
					
					<p class="text-center" style="color:black;font-weight: bold;">
						Ademas nuestro equipo de trabajo con experiencia en el desarrollo de proyectos de robotica que te ayudaran con la construccion de todo tipo de robots y circuitos.

					</p>
					
				</div>	

			-->


				<div class="col-md-4  col-sm-4 col-xs-4d one">
					<div class="up-border text-center">

								
							<a class="w3_play_icon1" href="#" data-toggle="modal" data-target="#myModal1"><img src="../images/clases.jpg" class="img-rounded" alt="Clases de refuerzo"></a>
							<h4>Clases de refuerzo</h4>
					
					</div>

				</div>
				<div class="col-md-4  col-sm-4 col-xs-4d two" style="height: 110%;">
					<div class="up-border text-center">
							
					<a class="w3_play_icon1" href="#" data-toggle="modal" data-target="#myModal2"><img src="../images/proyectos.jpg" class="img-c" alt="Tareas y trabajos"></a>
							<h4>Tareas y trabajos</h4>

					
	
					</div>

				</div>
				





				<div class="col-md-4 col-sm-4 our-advantages-grid three ">
					<div class="up-border text-center">

					<a class="w3_play_icon1" href="#" data-toggle="modal" data-target="#myModal3"><img src="../images/proyectos1.jpg" class="img-rounded" alt="Proyectos"></a>
							<h4>Proyectos</h4>

						
					
					</div>
					
				</div>

				
		</div>
	</div>

	
</div>

	<!--contact-->
	<div class="contact" id="contact">
		<div class="container">
			<h1 class="title" id="como" style="color: black"><span class="fa fa-cog"></span> Asi funciona WildKath </h1>
			<hr class="style18">
			
		 <div class="agileits-banner-grids text-center" >

				<div class="banner-bottom-girds text-center">
				
					<div class="col-md-4 col-sm-4 clr13 agileits-banner-grid text-center ">

						<span style="color: black;font-weight: bold;" class="text-left">1</span>
						<p><span class="fa fa-child banner-icon" aria-hidden="true"></span></p>

					
				
						<h4 style="color:black"> Comunicate con nosotros</h4>
						<p style="color:#696A6C"> Usa nuestros canales de comunicación para contactarnos si piensas que podemos ayudarte.</p>


						
					</div>

					<div class="col-md-4 col-sm-4 clr13 agileits-banner-grid text-center ">
						<span style="color: black;font-weight: bold;">2</span>
						<p><span class="fa fa-upload banner-icon" aria-hidden="true"></span></p>
						<h4 style="color:black;"> Envianos tus tareas o proyectos</h4>
						<p style="color: #696A6C;">Obtén tu cotización y confírmanos para que empecemos con el desarrollo de tu trabajo.</p>
						
					</div>


					<div class="col-md-4 col-sm-4 clr13 agileits-banner-grid text-center ">
						<span style="color: black;font-weight: bold;">3</span>
						<p><span class="fa fa-cogs banner-icon" aria-hidden="true"></span></p>


						<h4 style="color:black"> Nosotros nos encargamos</h4>
						<p style="color:#696A6C"> Cuando este listo nos pondremos de acuerdo para la entrega de tus trabajos o proyectos.</p>
					
					</div>



				
			</div>

			</div>
		</div>

	</div>


	<!-- gallery -->
	<div class="gallery " id="gallery" style=" background-color: rgba(175, 87, 188, 1);">

		<div class="container">
		<div class="row">
		<div class="col-md-12" style="height: 20%">
		<h2 class="title"><span style="color:white">Nuestra galeria de proyectos</span></h2>
		

			<!-- <div class="flexslider">
								  <ul class="slides">
								    <li data-thumb="slide1-thumb.jpg">
								      <img src="../images/slide1.jpg" />
								    </li>
								    <li data-thumb="slide2-thumb.jpg">
								      <img src="../images/slide2.jpg" />
								    </li>
								    <li data-thumb="slide3-thumb.jpg">
								      <img src="../images/slide3.jpg" />
								    </li>
								    <li data-thumb="slide4-thumb.jpg">
								      <img src="../images/slide4.jpg" />
								    </li>
								     <li data-thumb="slide5-thumb.jpg">
								      <img src="../images/slide5.jpg" />
								    </li>
								     <li data-thumb="slide6-thumb.jpg">
								      <img src="../images/slide6.jpg" />
								    </li>
								     <li data-thumb="slide7-thumb.jpg">
								      <img src="../images/slide7.jpg" />
								    </li>
								  </ul>
								</div>

				</div>
			</div>
	-->
		</div>
	

				
	 <div class="gallery-grids-top" >

	 <div class="container">
	 <div class="row">
	 <div class="col-md-12 col-md-offset-0">
				<div class="gallery-grids">

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img">
						<a class="example-image-link w3-agilepic" href="images/slide1.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide1.jpg" alt=""/> 

						</a>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img">
						<a class="example-image-link w3-agilepic" href="images/slide2.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide2.jpg" alt=""/> 
						 
						</a>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide3.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide3.jpg" alt=""/> 
							
						</a>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide4.jpg" data-lightbox="example-set" data-title="">
						<img class="example-image img-responsive" src="images/slide4.jpg" alt=""/>
							 
						</a>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide5.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide5.jpg" alt=""/> 
						
						</a>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide6.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide6.jpg" alt=""/>
					
						</a>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide7.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide7.jpg" alt=""/>
					
						</a>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide8.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide8.jpg" alt=""/>
					
						</a>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide9.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide9.jpg" alt=""/>
					
						</a>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide10.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide10.jpg" alt=""/>
					
						</a>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide11.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide11.jpg" alt=""/>
					
						</a>
					</div>

					<div class="col-md-2 col-sm-4 col-xs-6 gallery-grid-img ">
						<a class="example-image-link w3-agilepic" href="images/slide12.jpg" data-lightbox="example-set" data-title="">
							<img class="example-image img-responsive" src="images/slide12.jpg" alt=""/>
					
						</a>
					</div>





					<div class="clearfix"> </div>
				</div>
			</div>
			</div>
			</div>
			</div>


		</div>


	</div>

	<!--<div class="contact-top">
	<h1 class="text-center">Estamos localizados en:</h1>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d18929.654016255514!2d-73.6457373133541!3d4.12761649826717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3e31e1fea8f48b%3A0x9aebba968cf62e8a!2sWildKath!5e0!3m2!1ses!2sco!4v1520309035435" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
-->
	</div>
	<!--//contact-->
	<!--footer-->
	<div class="buttom" id="contacto">
		<div class="container">
		
	
			<div class="col-md-4 col-sm-4 mid-row">
				<h4>Síguenos</h4>
				<div class="up-out">
					<p>Conoce nuestras redes sociales y mantente conectado.
					</p>
					<div class="social-icon">

						<span class="fa fa-facebook font-icon" aria-hidden="true"></span>
						<span class="fa fa-twitter font-icon" aria-hidden="true"></span>
						<span class="fa fa-google-plus font-icon" aria-hidden="true"></span>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 text-center" id="cuete">
	<img src="../images/cuete2.gif">	
	</div>	

			<div class="col-md-4 col-sm-4 buttom-down three-rows" >
				<h4>Contáctanos:</h4>
				<div class="addres up-out">
					<p><span class="fa fa-map-marker icons-left" aria-hidden="true"></span>Carrera 60 Sur 6A- 15 Segundo Piso Barrio Las Americas
						

					<p><span class="fa fa-phone icons-left" aria-hidden="true"></span>Llámanos:<span class="number">3209720220</span></p>

					<p><span class="fa fa-envelope icons-left" aria-hidden="true"></span>Escríbenos:<span class="number"><a href="mailto:wildkath.ingenieria@gmail.co" class="info">wildkath.ingenieria@gmail.com</a></span></p>

				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

	<!--//footer-->

	<!--js working-->
	<script type='text/javascript' src='js/jquery-2.2.3.min.js'></script>
	<script type="text/javascript" src="js/modernizr-2.6.2.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<!-- //js  working-->
	<!--FlexSlider-->

	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">

		$(window).load(function() {
			  $('.flexslider').flexslider({
			    animation:  Modernizr.touch ? "slide" : "fade",
			    smoothHeight:true

			  });
			  $(".loader").fadeOut("slow");
			
						    $('.flexslider1').flexslider({
				    animation: "slide",
				    controlNav: "thumbnails"
				  });
			});



	</script>
	<!--End-slider-script-->
	<!--gallery script-->
	<script src="js/lightbox-plus-jquery.min.js"></script>
	<!--//gallery script-->


	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({ scrollTop: $(this.hash).offset().top }, 900);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
$(window).scroll(function() {

		$('#como').each(function(){
			var imagePos = $(this).offset().top;
			
			var topOfWindow = $(window).scrollTop();
				if (imagePos < topOfWindow+400) {
					$(this).addClass("slideExpandUp");
				}
			});	

$('#como3').each(function(){
			var imagePos = $(this).offset().top;
			
			var topOfWindow = $(window).scrollTop();
				if (imagePos < topOfWindow+400) {
					$(this).addClass("slideExpandUp");
				}
			});	

$('#como1').each(function(){
		var imagePos = $(this).offset().top;
			
			var topOfWindow = $(window).scrollTop();
				if (imagePos < topOfWindow+400) {
					$(this).addClass("slideUp");
				}
			});



});	
	


	


	</script>
	<!-- //here ends scrolling icon -->
</body>

</html>